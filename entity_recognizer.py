import requests
import ast

def recognize_known_entities(chat_context):
    
    duckling_entites = requests.post('http://34.203.252.121:8000/parse', data={'locale': 'en_BG', 'text': chat_context}).json()
    time_entities = []
    for entity in duckling_entites:
        if entity['dim'] == 'time':
            time_entities.append(entity['value']['value'])    
    return time_entities


if __name__ == '__main__':
    print(recognize_known_entities('Candidate is a Major at  Applied Genetics Bangalore University  during  June 2008___May 2011'))