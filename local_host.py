#!flask/bin/python
from flask import Flask, jsonify
from flask import request
from flask_cors import CORS, cross_origin
# from tornado.wsgi import WSGIContainer
# from tornado.httpserver import HTTPServer
# from tornado.ioloop import IOLoop
from gtts import gTTS
import os


cwd = os.getcwd()
app = Flask(__name__)
cors = CORS(app)

print "started"
import json

@app.route("/")
@cross_origin()
def welcome():
	return "hello"

@app.route ('/skillextractor', methods=['POST'])
def skill_extractor():
	print "connected"
	features_1 = request.json
   	cur_text_1 = features_1['text']
	keywords = json.load(open('keywords_1.json'))
	lines = cur_text_1.split("\n")
	found_skills = {}
	for line in lines:
		if line == '':
			pass
		else :
			words = line.split(' ')
			for word in words :
				if keywords.has_key(word.capitalize()) or keywords.has_key(word.lower()) or keywords.has_key(word.upper()) :
					found_skills[word]=1
	return jsonify({"foundskills" :found_skills.keys()})


@app.route ('/texttospeech', methods=['POST'])
@cross_origin()
def getskills():
   features = request.json
   print type(features),features
   cur_text = features['text']
   tts = gTTS(text=cur_text,lang='en')  
   tts.save("resume.mp3")
   response_ = jsonify({'filePath': cwd+"/resume.mp3"})
   return response_

if __name__ == '__main__':
   # http_server = HTTPServer (WSGIContainer (app))
   # http_server.listen (8000)
   # IOLoop.instance ().start ()
   app.run('0.0.0.0', 8000)