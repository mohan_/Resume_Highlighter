#!flask/bin/python
from flask import Flask, jsonify
from flask import request
from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from gtts import gTTS
import requests
import os
import pickle
import ahocorasick as ahc
import datetime
from dateutil.parser import parse
import datetime



app = Flask(__name__)
stop_words = {'into', 'after', 'very', "wasn't", 'were', 'under', 'ours', 'our', "don't", 'me',
 'yourselves', 'they', "needn't", 'between', 'until', 'do', 'for', 'have', "won't", 'at', 'again', 
 'mustn', 'its', 'not', 'ourselves', 'hers', 'wasn', 'below', 'down', 'where', 'are', 'ain', "she's",
  'just', 'above', 'isn', 'you', 'more', 've', 'only', 'hadn', 'having', 'which', 'haven', 'him', 
  'own', 'nor', 'can', 'aren', "weren't", 'i', 'to', "hadn't", 're', 'was', 'yourself', 'themselves', 
  'my', 'we', 'it', 'he', 'any', 'with', 'll', 'once', 'a', 'by', 'such', 'of', 'does', 'had', 'when',
   "wouldn't", 'm', "you're", 'these', 'this', "hasn't", 't', 'here', 'out', 'now', "you've", 'most',
    'how', 'doesn', 'those', 'her', 'didn', 'your', 'mightn', 'why', 'myself', "couldn't", 'shouldn', 
    'during', 'yours', 's', 'that', 'd', 'against', 'ma', 'am', 'other', 'than', 'did', 'if', 'weren',
     'then', 'further', 'the', 'should', "mightn't", 'shan', 'be', 'wouldn', "aren't", 'she', 'who', 'but',
      'both', 'few', 'whom', 'won','their', 'y', "shan't", 'being', "shouldn't", 'too', 'itself', "didn't", 
      'has', 'himself', 'been', 'them', 'an', 'through', 'so', "you'll", "you'd", 'before', 'don', 'doing', 
      'his', 'as', 'o', 'some', "that'll", "haven't", "it's", 'is', 'while', 'and', 'from', "should've",
       'over', 'all', 'will', 'couldn', 'needn', 'on', 'each', "doesn't", 'off', 'about', "isn't", 'up',
        'no', 'hasn', 'because', 'theirs', 'herself', 'what', 'same', 'there', 'in', 'or', "mustn't"}
        
keywords = []
def get_keywords():
    print('Entered')
    output = {}
    homedir = os.path.expanduser("~")
    keywords_file = homedir + '/data/coreml/keywords.pickle'
    with open(keywords_file, 'rb') as handle:
        output = pickle.load(handle)
    return output


output  = get_keywords()
if output:
    for row in output[0]:
        if not ' '+row['Keyword'].lower()+' ' in keywords:
            keywords.append((' '+row['Keyword'].lower()+' ',1))
    print(keywords[1])

def sorting(L):
     splitup = L.split('-')
     return splitup[1], splitup[0]

    
def convertTextToSpeech(cur_text):
    tts = gTTS(text=cur_text,lang='en')   
    tts.save("resume.mp3")  
    return {'filePath': "resume.mp3"}

def make_aho_automaton(keywords):
    A = ahc.Automaton()  # initialize
    for (key, cat) in keywords:
       A.add_word(key, (cat, key)) # add keys and categories
    A.make_automaton() # generate automaton
    return A
    
def find_keywords(line, A):
    found_keywords = []
    for end_index, (cat, keyw) in A.iter(line):
        found_keywords.append(keyw)
    return found_keywords

A_spaces = make_aho_automaton(keywords)

def recognize_time_entities(chat_context):
    duckling_entites = requests.post('http://34.203.252.121:8000/parse', data={'locale': 'en_BG', 'text': chat_context}).json()
    time_entities = []
    for entity in duckling_entites:
        if entity['dim'] == 'time':
            cur_date =  parse(entity['value']['value'])
            #datetime.datetime.strptime(entity['value']['value'].split('T')[0], "YYYY-MM-DD").date()
            time_entities.append(str(cur_date.month) + '-' + str(cur_date.year))   
    return time_entities

    
def getskillsfromdescription(text):
    extracted_keywords=[]
    #stop_words = set(stopwords.words('english'))
    for line in text:
        for kw in find_keywords(line.replace(',',' ').replace('.',' ').lower(), A_spaces):
            if kw.strip() not in extracted_keywords and not kw.strip() in stop_words:
                extracted_keywords.append(kw.strip())
    sorted_extracted_keywords = sorted(extracted_keywords, key=len, reverse=True)
    final_keywords =[]
    for line in text:
        cur_line = line
        for skw in sorted_extracted_keywords:
            if skw.strip().lower() in cur_line.strip().lower():
                cur_line = cur_line.lower().replace(skw.strip().lower(),' ')
                final_keywords.append(skw.strip())
    return final_keywords

@app.route ('/ner', methods=['POST'])
def get_resume_entities():

    features = request.json
    cur_text = features['text']
    url = 'http://174.129.112.60/?properties=%7B%22annotators%22%3A%20%22tokenize%2Cssplit%2Cner%22%2C%20%22date%22%3A%20%222018-04-07T19%3A53%3A11%22%7D&pipelineLanguage=en'
    headers = {'Content-Type': 'application/json'}
    #print(get_elastic_query())
    myResponse = requests.post(url, json=cur_text, headers=headers)
    out = myResponse.json()
    list_experiences = []
    cur_exp  = {}
    date_pair  = 0
    number_pair = 0
    skillsfromtext_input = []
    skillsfromtext_input.append(cur_text)
    final_skills = getskillsfromdescription(skillsfromtext_input)
    #print(final_skills)


    candidate_name = 'Candidate'
    candidate_email = ''
    sent_cout = 0
    for sent in out['sentences']:
        
        for entity in sent['entitymentions']:
            if sent_cout < 5:
                if entity['ner'] == 'EMAIL':
                    candidate_email == entity['text']
                if entity['ner'] == 'PERSON':
                    candidate_name = entity['text']
            #print(entity['ner'])
            if entity['ner'] == 'ORGANIZATION':
                if not cur_exp.get('ORGANIZATION',None):
                    cur_exp['ORGANIZATION'] = {}
                    cur_exp['ORGANIZATION']['text'] = entity['text']
                else:
                    list_experiences.append(cur_exp)
                    cur_exp = {}
                    cur_exp['ORGANIZATION'] = {}
                    cur_exp['ORGANIZATION']['text'] = entity['text']
            if entity['ner'] == 'TITLE':
                if not cur_exp.get('TITLE',None):
                    cur_exp['TITLE'] = {}
                    cur_exp['TITLE']['text'] = entity['text']
                else:
                    list_experiences.append(cur_exp)
                    cur_exp = {}
                    cur_exp['TITLE'] = {}
                    cur_exp['TITLE']['text'] = entity['text']                    
            if entity['ner'] == 'DATE':
                if not cur_exp.get('DATE',None):
                    #print(cur_exp)
                    #print('entered')c
                    cur_exp['DATE'] = {}
                    cur_exp['DATE']['text'] = entity['text']
                    #print(cur_exp , date_pair)
                elif date_pair == 0 :
                    date_pair = 1
                    #print('entered')
                    cur_exp['DATE']['text'] = cur_exp['DATE']['text'] + '___' + entity['text']
                else:
                    list_experiences.append(cur_exp)
                    cur_exp = {}
                    cur_exp['DATE'] = {}
                    cur_exp['DATE']['text'] = entity['text']
                    date_pair = 0  
                    number_pair = 0             
            if entity['ner'] == 'NUMBER':
                if not cur_exp.get('NUMBER',None):
                    #print(cur_exp)
                    #print('entered')
                    cur_exp['NUMBER'] = {}
                    cur_exp['NUMBER']['text'] = entity['text']
                elif number_pair == 0 :
                    number_pair = 1
                    cur_exp['NUMBER']['text'] = cur_exp['NUMBER']['text'] + '___' + entity['text']
                else:
                    list_experiences.append(cur_exp)
                    cur_exp = {}
                    cur_exp['NUMBER'] = {}
                    cur_exp['NUMBER']['text'] = entity['text']
                    number_pair = 0
                    date_pair = 0
    #print(list_experiences)
    
    experiences = {}

    for exp in list_experiences:
        if exp.get('DATE') or exp.get('NUMBER'):
            if exp.get('ORGANIZATION') and exp.get('TITLE'):
                if exp.get('DATE'):
                    text = exp['TITLE']['text'] + '|' +exp['ORGANIZATION']['text']+ '|' + exp['DATE']['text']
                    dates = recognize_time_entities(text)
                    for date in dates:
                        if not experiences.get(str(date)):
                            experiences[str(date)] = text
                    #print (text)
                else:
                    text = exp['TITLE']['text'] + '|' +exp['ORGANIZATION']['text']+ '|' +exp['NUMBER']['text'] 
                    dates = recognize_time_entities(text)
                    for date in dates:
                        if not experiences.get(str(date)):
                            experiences[str(date)] = text
                    #print (text)
    #print(experiences)
    all_dates = list(experiences.keys())
    all_dates = sorted(all_dates, key=sorting)
    #print(all_dates)
    #sorted(all_dates, key=lambda x: datetime.datetime.strptime(x, '%m-%Y'))
    #sorted(all_dates, key=lambda d: map(int, d.split('-')))
    text = 'Hey the name of the candidate is ' + candidate_name
    latest_title = experiences[all_dates[len(all_dates) -1]].split('|')[0]
    latest_company = experiences[all_dates[len(all_dates) -1]].split('|')[1]
    text = text + '. His latest title is ' + latest_title +' and he has recently worked at ' + latest_company
    
    text = text + '. Some of his skills are ' +final_skills[0] + ', ' + final_skills[1]+  ', ' +final_skills[2] +  ', ' + final_skills[3] + ' etc... Thank you.' 
    print(text)
    path = convertTextToSpeech(text)
    return jsonify(path)



if __name__ == '__main__':
    http_server = HTTPServer (WSGIContainer (app))
    http_server.listen (5000)
    IOLoop.instance ().start ()